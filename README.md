<VirtualHost *:80>
        DocumentRoot "/var/www/Php"
        ServerName php.loc
        DirectoryIndex index.php

        <Directory "/var/www/Php">
                Options All
                AllowOverride All
                Require all granted
        </Directory>

</VirtualHost>

<VirtualHost *:80>
        DocumentRoot "/var/www/UvodUPhp"
        ServerName uvoduphp.loc
        DirectoryIndex index.html

        <Directory "/var/www/UvodUPhp">
                Options All
                AllowOverride All
                Require all granted
        </Directory>

</VirtualHost>

<VirtualHost *:80>
        DocumentRoot "/var/www/VirtualExampleThree"
        ServerName virtualhostthree.loc
        DirectoryIndex index.html

        <Directory "/var/www/VirtualExampleThree">
                Options All
                AllowOverride All
                Require all granted
        </Directory>

</VirtualHost>

<VirtualHost *:80>
	DocumentRoot "/var/www/VirtualExampleTwo"
	ServerName virtualhosttwo.loc
	DirectoryIndex index.html

	<Directory "/var/www/VirtualExampleTwo">
		Options All
		AllowOverride All
		Require all granted
	</Directory>

</VirtualHost>
